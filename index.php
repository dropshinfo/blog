<?php

$config = include_once("config.php");
$arrPosts = [];
$filename = "db/post.json";

$page = 1;
$pageSize = 3;

$fil=false;

if(isset($_GET['page']) && is_numeric($_GET['page']) && $_GET['page'] >0 )
{
    $page = $_GET['page'];
}

if (file_exists($filename)) 
{
   $fil=true;   
   $f = fopen($filename, "r");
    
    $offset = ($page - 1) * $pageSize;
    $end = $offset + $pageSize;
    
    for($i = 0;$i<$offset;$i++) 
	{
        fgets($f);
    }
    
    for ($i = $offset; ($i < $end && !feof($f)); $i++) 
	{
        $tmp = json_decode(fgets($f), true);
        if (!is_null($tmp)) 
		{
            $arrPosts[] = $tmp;
        }
    }
$count=count(file($filename));//кол-во постов
$pages=ceil($count/$pageSize);//кол-во страниц
}



include_once("tpl/index.php");
?>