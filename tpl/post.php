<!DOCTYPE html>
<html>
<head lang="en">
	<?php $config=require_once('config.php'); ?>
	<meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <title><?php echo $config['nameblog']?></title>
</head>
<body>
    <?php foreach($postArr as $value){ ?>
	<h1 class="main-title"><?php echo $value['title']; ?></h1>
    <div class="container-fluid">
        <div class="col-md-3">
            <ul class="nav nav-pills nav-stacked">
                <?php require_once('menu.php'); ?>
            </ul>
        </div>
        <div class="col-md-9 blog-body">
        
            <div class="post">
                <h2 class="post-title"> <?php $value['title']; ?></h2>
                <h3 class="post-subtitle">
                   <?php echo $value['body']; ?>
				</h3>
                    
                <p class="post-meta"><span class="glyphicon glyphicon-time"></span> Posted by <a href="#">Start Bootstrap</a> <?php echo $value['date']; } ?>
                </p>
                
                <hr />
            </div>
            <a href="dellete.php?id=<?php echo $value['id']; ?>" class="btn btn-primary btn-sm pull-right">Удалить запись</a>  
        </div>    
        
    </div>
</body>
</html>