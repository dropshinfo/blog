<!DOCTYPE html>
<html>
<head lang="ru">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <title><?php $config=require_once("config.php"); $config['nameblog']; ?></title>
</head>
<body>
    <h1 class="main-title"></h1>
    <div class="container-fluid">
        <div class="col-md-3">
 <?php require_once'menu.php'; ?>
        </div>
        <div class="col-md-9 blog-body">
            <?php foreach ($arrPosts as $line) { ?>
            <div class="post">
                <h2 class="post-title"><?php echo $line['title']; ?></h2>
                <h3 class="post-subtitle">
                    <?php echo $line['summary']; ?>
                </h3>
                    
                <p class="post-meta"><span class="glyphicon glyphicon-time"></span> <?php echo $line['date']; ?>
                    <a href="readmore.php?id=<?php echo $line['id']; ?>" class="btn btn-primary btn-sm pull-right">Читать полностью</a>
                </p>
                
                <hr />
            </div>
            <?php } ?>
            
            
            <ul class="pagination pull-right" boundary-links="true">
                <?php 
				    if($fil==true)
					{
					for($i = $page - 3; $i < $page + 3; $i++)
					{
                      if ( $i>=1 && $i <= $pages ) 
					  {
                         if ( $i == $page ) 
						 {
                           echo "<li class=\"active\"><a href=\"?page=$i\" class=\"ng-binding\">$i</a></li>";
                         }
                        else 
					    {
					       echo "<li><a href=\"?page=$i\" class=\"ng-binding\">$i</a></li>";
					    }
                      }
                    } 
					}
				?>
				   
			
				
            </ul>
        </div>    
        
    </div>
</body>
</html>