<?php

$config=require_once 'config.php';
require_once 'tpl/create.php';

$id = sha1(time() . mt_rand(0, 10000));
if($_POST['title']!="" && $_POST['summary']!="")
{
  $post=array('id'=>$id,'title'=>$_POST['title'],'summary'=>$_POST['summary'],'date'=>date('m.d.y h.i.s'));
  $jsonpost=json_encode($post);



  file_put_contents('db/post.json',"$jsonpost \n",FILE_APPEND);

  $postName=$id.'.json';
  $post['body']=$_POST['body'];
  $jsonBody=json_encode($post);
  file_put_contents("db/posts/$postName",$jsonBody);
  
  unset ($_POST['title']);
  unset ($_POST['summary']);
}
?>